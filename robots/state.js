const fs = require('fs');

const contentFilePath = './content.json';
const historicalFilePath = './historical.json';

function save(content) {
    const contentString = JSON.stringify(content);
    return fs.writeFileSync(contentFilePath, contentString);
}

function load() {
    const fileBuffer = fs.readFileSync(contentFilePath, 'utf-8');
    const contentJson = JSON.parse(fileBuffer);
    return contentJson;
}

function saveHistorical(content) {
    const contentString = JSON.stringify(content);
    return fs.writeFileSync(historicalFilePath, contentString);
}

function loadHistorical() {
    const fileBuffer = fs.readFileSync(historicalFilePath, 'utf-8');
    const contentJson = JSON.parse(fileBuffer);
    return contentJson;
}

module.exports = {
    save,
    load,
    saveHistorical,
    loadHistorical
};
