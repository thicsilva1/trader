class Paper{
  constructor(price, volume, date){
    this.price = Number.parseFloat(price);
    this.volume = Number.parseFloat(volume);
    this.date = new Date(date * 1000);
  }

  getPrice(){
    return this.price;
  }

  getVolume(){
    return this.volume;
  }

  getDate(){
    return this.date;
  }

  isSamePeriod(otherDate){
    otherDate = new Date(otherDate * 1000);
    const referenceDate = new Date(this.getDate() + 5 * 60000);
    return otherDate<=referenceDate;
  }
}

module.exports = Paper;