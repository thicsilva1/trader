const Candle = require("./Candle");

class CandleFactory {
  createToDate(date, papers) {
    let max = Number.MIN_VALUE;
    let min = Number.MAX_VALUE;
    let open = papers.length ? papers[0].getPrice() : 0;
    let close = papers.length ? papers[0].getPrice() : 0;
    let volume = 0;

    for (const paper of papers) {
      volume += paper.getVolume();
      let price = paper.getPrice();
      if (price > max) {
        max = price;
      }
      if (price < min) {
        min = price;
      }
    }

    open = papers[0].getPrice();
    close = papers[papers.length - 1].getPrice();
    volume = volume / papers.length;

    return new Candle(open, close, min, max, volume, date);
  }

  createCandle(allPapers, interval, units) {
    const candles = [];
    let papersFromPeriod = [];
    let actualPeriod = allPapers[0].getDate();
    let comparePeriod = this.dateAdd(actualPeriod, interval, units);

    for (const paper of allPapers) {
      if (paper.getDate().getTime() > comparePeriod.getTime()) {
        let actualCandle = this.createToDate(actualPeriod, papersFromPeriod);
        candles.push(actualCandle);
        papersFromPeriod = [];
        actualPeriod = paper.getDate();
        comparePeriod = this.dateAdd(actualPeriod, interval, units);
      }
      papersFromPeriod.push(paper);
    }
    let actualCandle = this.createToDate(actualPeriod, papersFromPeriod);
    candles.push(actualCandle);

    return candles;
  }

  dateAdd(date, interval, units) {
    if (!(date instanceof Date)) {
      return undefined;
    }

    var ret = new Date(date);
    var checkRollOver = function () {
      if (ret.getDate() != date.getDate()) ret.setDate(0);
    };
    switch (String(interval).toLowerCase()) {
      case "year":
        ret.setFullYear(ret.getFullYear() + units);
        checkRollOver();
        break;
      case "quarter":
        ret.setMonth(ret.getMonth() + 3 * units);
        checkRollOver();
        break;
      case "month":
        ret.setMonth(ret.getMonth() + units);
        checkRollOver();
        break;
      case "week":
        ret.setDate(ret.getDate() + 7 * units);
        break;
      case "day":
        ret.setDate(ret.getDate() + units);
        break;
      case "hour":
        ret.setTime(ret.getTime() + units * 3600000);
        break;
      case "minute":
        ret.setTime(ret.getTime() + units * 60000);
        break;
      case "second":
        ret.setTime(ret.getTime() + units * 1000);
        break;
      default:
        ret = undefined;
        break;
    }
    return ret;
  }
}

module.exports = CandleFactory;
