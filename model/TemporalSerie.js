class TemporalSerie {
  constructor(candles){
    this.candles = candles
  }

  getCandle(i){
    return this.candles[i];
  }

  getTotal(){
    return this.candles.length;
  }
}

module.exports = TemporalSerie;