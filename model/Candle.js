class Candle{
  constructor(open, close, min, max, volume, date){
    this.open = Number.parseFloat(open);
    this.close = Number.parseFloat(close);
    this.min = Number.parseFloat(min);
    this.max = Number.parseFloat(max);
    this.volume = Number.parseFloat(volume);
    this.date = date.getTime();
  }

  getOpen(){
    return this.open;
  }

  getClose(){
    return this.close;
  }

  getMin(){
    return this.min;
  }

  getMax(){
    return this.max;
  }

  getVolume(){
    return this.volume;
  }

  getDate(){
    return this.date;
  }

  isUp(){
    return this.open < this.close;
  }

  isDown(){
    return this.open > this.close;
  }
}

module.exports = Candle;