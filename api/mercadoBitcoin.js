const axios = require('axios');

module.exports={
	ticker(){
		return this.call('ticker');
	},

	trades(){
		return this.call('trades');
	},
	daySummary(){
		return this.call('day-summary')
	},

	async call(method){
		let url = `https://www.mercadobitcoin.net/api/BTC/${method}`;
		if (method==='trades'){
			const initialDate = new Date();
			initialDate.setDate(initialDate.getDate()-1);
			const finalDate = new Date();
			const initialTimestamp = Math.round(initialDate.getTime() / 1000);
			const finalTimestamp = Math.round(finalDate.getTime() / 1000);
			url+=`/${initialTimestamp}/${finalTimestamp}`;			
		}
		if (method==='day-summary'){
			const initialDate = new Date();
			
			initialDate.setDate(initialDate.getDate()-1);						
			url+=`/${initialDate.getFullYear()}/${initialDate.getMonth()+1}/${initialDate.getDate()}`
		}
		try {			
			const response = await axios.get(url);
			return response.data;
		} catch (error) {
			console.log(error)
			return;
		}
	}
}
