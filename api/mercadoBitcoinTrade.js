const axios = require('axios');
const crypto = require('crypto');
const qs = require('querystring');
require('dotenv-safe').load();

module.exports={
    getAccountInfo(){
        return this.call('get_account_info', {});
    },
    listOrders(params){
        return this.call('list_orders', params);
    },
    placeBuyOrder(qty, limit_price){
        return this.call('place_buy_order', {
            coin_pair: `BRLBTC`,
            quantity: ('' + qty).substr(0, 10),
            limit_price: '' + limit_price
        })
    },
    placeSellOrder(qty, limit_price){
        return this.call('place_sell_order', {
            coin_pair: `BRLBTC`,
            quantity: ('' + qty).substr(0, 10),
            limit_price: '' + limit_price
        })
    },
    listSystemMessages(){
        return this.call('list_system_messages', {});
    },

    async call(method, params){
        const endpoint = 'https://www.mercadobitcoin.net/tapi/v3/';
        const path = '/tapi/v3/';        
        const key= process.env.KEY;
        const secret= process.env.SECRET;
        const pin= process.env.PIN;
        const nonce = Math.round(new Date().getTime());        
        let querystring = qs.stringify({
            tapi_method: method,
            tapi_nonce: nonce
        });
        if (params) {
            querystring += '&' + qs.stringify(params);
        }
        const signature = crypto
            .createHmac('sha512', secret)
            .update(path + '?' + querystring)
            .digest('hex');
        try {

            const response = await axios.post(endpoint, querystring, {
                    headers: {
                        'TAPI-ID': key,
                        'TAPI-MAC': signature
                    }
                });
                        
    
            if (response.data) {
                if (response.data.status_code === 100 ) {
                    return response.data.response_data;
                } else if (response.data.status_code>=199 && response.data.status_code<=500) {                    
                    return response.data.error_message;
                } else {                    
                    return response.data;
                }
            } else {                
                return response;
            }
        } catch (error){
            console.log(error);
            return;
        }
    }
}