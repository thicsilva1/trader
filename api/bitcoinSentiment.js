const axios = require('axios');

module.exports = {
	async getSentiment(){
		try {
			const response = await axios.get('https://api.alternative.me/fng/');
			return response.data;
		} catch (error) {
			console.log(error);
			return;
		}
	}
}
// const axios = require('axios');

// class BitcoinSentiment {
// 	constructor() {
// 		this.endpoint = 'https://api.alternative.me/fng/';
// 	}
// 	getSentiment(success) {
// 		axios
// 			.get(this.endpoint)
// 			.then((result) => {
// 				success(result.data);
// 			})
// 			.catch((error) => console.log(error));
// 	}
// }

// module.exports = BitcoinSentiment;
