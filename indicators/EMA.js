class EMA {
  calculate(position, serie, size){
    let total = 0;
    for (let i=position -size; i <= position; i-- ){
      let candle = serie.getCandle(i);
      total+=candle.getClose();
    }
    return total / size;
  }
}