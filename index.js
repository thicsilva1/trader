const http = require('http');
http
    .createServer((request, response) => {
        response.writeHead(200, { 'Content-Type': 'json' });
        response.end('Tudo ok', 'utf-8');
    })
    .listen(process.env.PORT || 5000);

require('dotenv-safe').load();

const state = require('./robots/state');
const infoApi = require('./api/mercadoBitcoin');
const tradeApi = require('./api/mercadoBitcoinTrade');

const sentimentApi = require('./api/bitcoinSentiment');

async function getQuantity(price, isBuy) {
    price = parseFloat(price);
    coin = isBuy ? 'brl' : 'btc';
    try {

        const accountInfo = await tradeApi.getAccountInfo();
        let balance = parseFloat(accountInfo.balance[coin].available).toFixed(5);
        balance = parseFloat(balance);

        if (isBuy && balance < 30) {
            console.log('Sem saldo disponível para comprar');
            return balance;
        }

        console.log(`Saldo disponível de ${coin}:${balance}`);

        if (isBuy) {
            balance = parseFloat(balance / price).toFixed(5);
            const total = parseFloat(balance) - 0.00001;
            return total;
        }

        return balance;
    } catch (error) {
        console.log(error);
        return;
    }
}

let interval = setInterval(async function start() {

    // verificar média de preços
    const daySummary = await infoApi.daySummary();
    const avgPrice = daySummary.avg_price;

    const tickers = await infoApi.ticker();
    const ticker = tickers.ticker;
    const content = state.load();
    const oneDay = 1000 * 60 * 60 * 24;
    const tickerDate = ticker.date * 1000;
    const contentDateBuy = content.bestOfWeek.buy.date * 1000;
    const contentDateSell = content.bestOfWeek.sell.date * 1000;
    const diffInDaysBuy = Math.round((tickerDate - contentDateBuy) / oneDay);
    const diffInDaysSell = Math.round((tickerDate - contentDateSell) / oneDay);
    const startPrice = ticker.high - (ticker.high - ticker.low) / 2;

    if (ticker.low < content.bestOfWeek.buy.price || diffInDaysBuy > 3) {
        content.bestOfWeek.buy = {
            date: ticker.date,
            price: ticker.low
        };
        state.save(content);
    }

    if (ticker.high > content.bestOfWeek.sell.price || diffInDaysSell > 3) {
        content.bestOfWeek.sell = {
            date: ticker.date,
            price: ticker.high
        };
        state.save(content);
    }

    console.log(
        'Ven: ' + parseFloat(ticker.sell).toFixed(2),
        'Com: ' + parseFloat(ticker.buy).toFixed(2),
        'Últ: ' + parseFloat(ticker.last).toFixed(2),
        'Mín: ' + parseFloat(ticker.low).toFixed(2),
        'Max: ' + parseFloat(ticker.high).toFixed(2),
        'Avg: ' + parseFloat(avgPrice).toFixed(2)
    );

    console.log(
        'Melhor Compra: ' + parseFloat(content.bestOfWeek.buy.price).toFixed(2),
        'Melhor Venda: ' + parseFloat(content.bestOfWeek.sell.price).toFixed(2)
    );

    const orders = await tradeApi.listOrders({ coin_pair: 'BRLBTC' });
    const lastOrder = orders.orders ? orders.orders[0] : 0;
    console.log(lastOrder);
    // Compras
    if (lastOrder.order_type === 2 && lastOrder.status === 4) {

        if (ticker.sell < avgPrice) {

            const sentiment = await sentimentApi.getSentiment();

            if (sentiment.data[0].value <= 55) {
                const limitBuyValue = content.bestOfWeek.buy.price * parseFloat(process.env.PROFITBUY);
                const quantity = await getQuantity(ticker.sell, true);

                if (ticker.sell > ticker.last &&
                    ticker.sell < startPrice &&
                    (ticker.sell <= content.historicalBuy ||
                        (ticker.sell <= limitBuyValue && ticker.sell >= ticker.low))
                ) {
                    const trade = await tradeApi.placeBuyOrder(quantity, ticker.sell);
                    content.lastBuy = ticker.sell;
                    if (content.lastBuy < content.historicalBuy) {
                        content.historicalBuy = content.lastBuy;
                    }
                    state.save(content);
                    console.log('Ordem de venda adicionada no livro:' + trade.order);
                }

            } else {
                const nextTime = new Date().getTime() + sentiment.data[0].time_until_update * 1000;
                const nextDate = new Date(nextTime);

                console.log(`Mercado não está legal para compras. Vamos consultar novamente em ${nextDate}`);

                clearTimeout(interval);
                interval = setInterval(start, sentiment.data[0].time_until_update * 1000);
            }
        } else {
            console.log('Valor está muito alto, vamos esperar para comprar');
        }
        //Vendas
    } else if (lastOrder.order_type === 1 && lastOrder.status === 4) {
        const sellValue = (parseFloat(lastOrder.executed_price_avg) * parseFloat(process.env.PROFITSELL)).toFixed(5);
        if (
            ticker.buy < ticker.last &&
            ticker.buy > startPrice &&
            ticker.buy >= sellValue &&
            ticker.buy <= ticker.high
        ) {
            const quantity = await getQuantity(ticker.buy, false);
            const trade = await tradeApi.placeBuyOrder(quantity, ticker.buy);
            content.lastSell = ticker.buy;
            if (content.lastSell > content.historicalSell) {
                content.historicalSell = content.lastSell;
            }
            state.save(content);
            console.log('Ordem de venda adicionada no livro:' + trade.order);
        } else {
            console.log('Valor está muito abaixo do esperado para venda');
            content.lastPrice = ticker.buy;
            state.save(content);
        }
    } else if (!lastOrder) {
        console.log(orders);
    } else {
        console.log('Ordens em aberto, aguarde...');
    }

}, process.env.CRAWLER_INTERVAL);