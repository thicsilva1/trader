const state = require("./robots/state");
const infoApi = require("./api/mercadoBitcoin");
const CandleFactory = require("./model/CandleFactory");
const Paper = require("./model/Paper");
const papers = [];
let candles = [];
setInterval(() => {
  infoApi.ticker().then((tickers) => {
    papers.push(
      new Paper(tickers.ticker.sell, tickers.ticker.vol, tickers.ticker.date)
    );
    let factory = new CandleFactory();
    candles = factory.createCandle(papers, "second", 5);
    
  });
  console.log(candles);
}, 1000);