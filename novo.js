const axios = require("axios");

async function getDaySumary(date) {
  let url = `https://www.mercadobitcoin.net/api/BTC/day-summary/${date[0]}/${date[1]}/${date[2]}`;
  const response = await axios.get(url);
  return response.data;
}

function calculateRsi(sample) {
  let down = 0;
  let up = 0;

  sample.forEach((item, index)=>{
    if (index>0){

      const change = item.closing - sample[index-1].closing;
      if (change>0){
        up+=change;
        down+=0;
      } else if (change==0) {
        up+=0;
        down+=0;
      } else {
        up+=0;
        down+=Math.abs(change);
      }
    }
  });

  const avgUp = up / sample.length;
  const avgDown = down / sample.length;
  const rs = avgUp / avgDown;
  const rsi = 100 - 100 / (1+rs);
  console.log (avgUp, avgDown, rs, rsi);
  return rsi;
}

async function start() {
  const dataSample = [];

  initialDate = new Date();
  finalDate = new Date();
  initialDate.setDate(initialDate.getDate() - 14);
  i = 0;
  while (initialDate < finalDate) {
    initialDate.setDate(initialDate.getDate() - 14 + i);
    const dateArr = initialDate.toLocaleDateString().split("-");
    const response = await getDaySumary(dateArr);
    dataSample.push(response);
    i++;
  }

  for (i = 0; i < dataSample.length - 14; i++) {
    const sample = dataSample.slice(i, i + 14);
    calculateRsi(sample);
  }
}

start();
